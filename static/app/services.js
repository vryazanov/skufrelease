/**
 * Created by Вадим on 09.02.2015.
 */

angular.module('ReleaseApp')
    .factory('ReleaseList', function($resource){
        return $resource('/api/release/:releaseId', {releaseId:'@id'});
    });
