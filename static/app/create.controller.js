/**
 * Created by Вадим on 09.02.2015.
 */

angular.module('ReleaseApp')
    .controller('CreateController', ['$scope', '$cookies', '$location', 'FileUploader', function($scope, $cookies, $location, FileUploader) {
        var uploader = $scope.uploader = new FileUploader({queueLimit: 1, alias: 'def_file'});
        var formData = [];
        uploader.headers = {
            'X-CSRFToken': $cookies.csrftoken
        };

        $scope.createRelease = function() {
            formData.push({
                short_description: $scope.short_description,
                description: $scope.description,
                release_date: document.getElementById('date').value,
                automatically: $scope.automatically
            });
            uploader.uploadAll();
        }

        uploader.onBeforeUploadItem = function(item) {
            Array.prototype.push.apply(item.formData, formData);
        };

        uploader.onCompleteAll = function() {
            $location.path('/');
        };

    }]);