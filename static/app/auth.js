/**
 * Created by Вадим on 11.02.2015.
 */

angular.module('ReleaseApp')
    .factory('Auth', ['$http', '$window', 'User', '$location', function($http, $window, User, $location){

        function isLoggedIn() {
            User.username = $window.user.username;
            return !(User.username == 'AnonymousUser');
        };

        function Login(username, password) {
            $http.post('/api/auth/', {username: username, password: password}).success(function(data){
                console.log(data.username);
                User.username = data.username;
                $location.path('/');
            });
        };

        return {
            isLoggedIn: isLoggedIn,
            Login: Login
        }
    }]);

angular.module('ReleaseApp')
    .controller('AuthController', ['Auth', '$scope', '$location', 'User', function(Auth, $scope, $location, User){
        $scope.auth = function () {
            console.log($scope.username);
            Auth.Login($scope.username, $scope.password);
        };
    }]);
