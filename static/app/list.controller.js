/**
 * Created by Вадим on 09.02.2015.
 */

angular.module('ReleaseApp')
    .controller('ListController', [
        '$scope', 'ReleaseList',
        function($scope, ReleaseList){
            $scope.translate = {
                'In progress': ['Запланирован', 'info'],
                'Resolved': ['Выполнен', 'success'],
                'Error': ['Ошибка', 'error'],
                'Cancelled': ['Отменён', 'warning']
            }

            $scope.to_translate = function(status) {
                return $scope.translate[status][0];
            };

            $scope.set_class = function(status) {
                return $scope.translate[status][1];
            };

            $scope.isAutomatically = function (auto) {
                return auto === true;
            };

            $scope.list = ReleaseList.query();
    }]);