/**
 * Created by Вадим on 09.02.2015.
 */

angular.module('ReleaseApp', ['ngResource', 'angularFileUpload', 'ngCookies', 'ngRoute']);

angular.module('ReleaseApp')
    .config(['$routeProvider', '$locationProvider',
        function ($routeProvider, $locationProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: '/static/list.html',
                    controller: 'ListController'
                })
                .when('/create', {
                    templateUrl: '/static/create.html',
                    controller: 'CreateController'
                })
                .when('/login', {
                    templateUrl: '/static/login.html',
                    controller: 'AuthController'
                })
                .when('/release/:releaseId',{
                    templateUrl: '/static/detail.html',
                    controller: 'DetailController'
                });
        }]);

angular.module('ReleaseApp')
    .run(['$location', '$rootScope','Auth', 'User', function($location, $rootScope, Auth, User){
        console.log(Auth.isLoggedIn());
        //if (User.username == 'AnonymousUser') { $location.path('/login')};

        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            if ((User.username == 'AnonymousUser') && next.$$route.originalPath != '/login')
                $rootScope.$evalAsync(function() { $location.path('/login')});
        });
    }]);

angular.module('ReleaseApp')
    .value('User', {username: null});

angular.module('ReleaseApp')
    .controller('HeaderController', ['$scope', '$location', function($scope, $location){
        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };
    }]);

angular.module('ReleaseApp')
    .directive('datetimepicker', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
             link: function (scope, element, attrs, ngModelCtrl) {
                $(element).datetimepicker({
                    format: 'H:i d.m.y',
                    onSelect: function (date) {
                        scope.$apply();
                    }
                });
            }
        };
});

angular.module('ReleaseApp')
    .filter('nl2br', function($sce){
        return function (text) {
            return text ? $sce.trustAsHtml(text.replace(/\n/g, '<br/>')) : '';
        };
});