/**
 * Created by Вадим on 11.02.2015.
 */

angular.module('ReleaseApp')
    .controller('DetailController', [
        '$scope', '$routeParams', 'ReleaseList',
        function($scope, $routeParams, ReleaseList){
            //$scope.main = true;
            $scope.panel = 'main';

            $scope.translate = {
                'In progress': ['Запланирован', 'info'],
                'Resolved': ['Выполнен', 'success'],
                'Error': ['Ошибка', 'error'],
                'Cancelled': ['Отменён', 'warning']
            };

            $scope.to_translate = function(status) {
                if (!status) return;
                return $scope.translate[status][0];
            };

            $scope.changePanel = function (type) {
                $scope.panel = type;
            };

            ReleaseList.get({releaseId: $routeParams.releaseId}, function(release){
                $scope.detail = release;
            });

            $scope.isActive = function(type) {
                console.log($scope.panel == type);
                return $scope.panel == type;
            };
    }]);