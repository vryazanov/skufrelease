from rest_framework import serializers
from skufrelease.models import *


class ReleaseSerializer(serializers.ModelSerializer):
    release_date = serializers.DateTimeField(input_formats=('%H:%M %d.%m.%y',))

    class Meta:
        model = Release
        fields = ('id', 'owner', 'short_description','description',
                  'def_file', 'release_date', 'status', 'owner_fullname', 'automatically')
        write_only_fields = 'owner'