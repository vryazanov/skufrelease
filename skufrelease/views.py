from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from skufrelease.models import Release
from django.contrib.auth import authenticate, login
from skufrelease.serializers import *
# Create your views here.


class AuthView(APIView):
    def get(self, request, format=None):
        content = {
            'user': unicode(request.user),  # `django.contrib.auth.User` instance.
        }
        return Response(content)

    def post(self, request, format=None):
        try:
            username, password = request.data['username'], request.data['password']
        except KeyError:
            return Response({'detail': 'KeyError exception'}, status=status.HTTP_400_BAD_REQUEST)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return Response({'detail': 'User logged in.'})
            else:
                return Response({'detail': 'Disabled account'})
        return Response({'detail': 'Username or password incorrect.'})


class ReleaseListView(generics.ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Release.objects.all()
    serializer_class = ReleaseSerializer

    def post(self, request, format=None):
        request.DATA[u'owner'] = request.user.pk
        serializer = ReleaseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReleaseDetailView(generics.RetrieveDestroyAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Release.objects.all()
    serializer_class = ReleaseSerializer
    lookup_field = 'id'