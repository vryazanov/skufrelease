from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from datetime import datetime
import os
# Create your models here.


def get_release_path(instance, filename):
    return os.path.join('releases', datetime.now().strftime('%d.%m.%y'), filename)


class Release(models.Model):
    RESOLVED = 'Resolved'
    IN_PROGRESS = 'In progress'
    ERROR = 'Error'
    CANCELLED = 'Cancelled'
    STATUS = (
        (RESOLVED, 'Resolved'),
        (IN_PROGRESS, 'In progress'),
        (ERROR, 'Error'),
        (CANCELLED, 'Cancelled')
    )

    owner = models.ForeignKey(User)
    short_description = models.CharField(max_length=50)
    description = models.TextField()
    def_file = models.FileField(upload_to=get_release_path)
    release_date = models.DateTimeField()
    status = models.CharField(max_length=15, choices=STATUS, default=IN_PROGRESS)
    automatically = models.BooleanField(default=False)

    @property
    def owner_fullname(self):
        return self.owner.get_full_name()

    def __str__(self):
        return '%s | %s' % (self.status, self.release_date)