import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True

TEMPLATE_DEBUG = True

STATIC_ROOT = None

STATICFILES_DIRS = (
    os.path.join(BASE_DIR,  '../static'),
)