from django.conf.urls import patterns, include, url
from django.contrib import admin
from skufrelease.views import *
from django.views.generic import TemplateView
from skufrelease.settings import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'SKUFProject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^api/release/(?P<id>[0-9]+)', ReleaseDetailView.as_view()),
    url(r'^api/release/', ReleaseListView.as_view()),
    url(r'^api/auth/', AuthView.as_view()),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name="home"),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),)