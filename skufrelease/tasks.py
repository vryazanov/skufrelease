# coding: utf-8
from celery.task import task
from pyars import erars, cars
from skufrelease.models import *
from zipfile import ZipFile
from datetime import datetime
import django
import os

django.setup()


@task
def install_release():
    release_list = Release.objects.filter(release_date__lte=datetime.now(),
                                          status=Release.IN_PROGRESS,
                                          automatically=True)

    if not release_list:
        return

    ars = erars.erARS()
    ars.Login('server', 'login', 'password')

    for release in release_list:
        with ZipFile(release.def_file.name, 'r') as zip:
            for file in zip.namelist():
                definitions = zip.open(file, 'r').read()
                error = ars.Import(cars.ARStructItemList(), definitions, cars.AR_IMPORT_OPT_OVERWRITE)
                if error:
                    break
        if error:
            release.status = Release.ERROR
        else:
            release.status = Release.RESOLVED
        release.save()



