# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skufrelease', '0003_auto_20150209_1616'),
    ]

    operations = [
        migrations.AddField(
            model_name='release',
            name='status',
            field=models.CharField(default=b'In progress', max_length=15, choices=[(b'Resolved', b'Resolved'), (b'In progress', b'In progress'), (b'Error', b'Error'), (b'Cancelled', b'Cancelled')]),
            preserve_default=True,
        ),
    ]
