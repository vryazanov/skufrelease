# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skufrelease', '0005_auto_20150209_2142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='description',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='release',
            name='short_description',
            field=models.CharField(max_length=50),
            preserve_default=True,
        ),
    ]
