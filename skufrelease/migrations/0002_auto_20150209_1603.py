# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skufrelease', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='release',
            name='backup_file',
            field=models.FileField(upload_to=b'files/backups/', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='release',
            name='def_file',
            field=models.FileField(upload_to=b'files/releases/'),
            preserve_default=True,
        ),
    ]
