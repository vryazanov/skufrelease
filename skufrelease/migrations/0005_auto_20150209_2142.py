# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skufrelease', '0004_release_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='release',
            name='short_description',
            field=models.TextField(default='without description'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='release',
            name='description',
            field=models.CharField(max_length=50),
            preserve_default=True,
        ),
    ]
