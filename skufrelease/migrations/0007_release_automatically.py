# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skufrelease', '0006_auto_20150209_2142'),
    ]

    operations = [
        migrations.AddField(
            model_name='release',
            name='automatically',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
