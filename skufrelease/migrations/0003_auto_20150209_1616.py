# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import skufrelease.models


class Migration(migrations.Migration):

    dependencies = [
        ('skufrelease', '0002_auto_20150209_1603'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='release',
            name='backup_file',
        ),
        migrations.AlterField(
            model_name='release',
            name='def_file',
            field=models.FileField(upload_to=skufrelease.models.get_release_path),
            preserve_default=True,
        ),
    ]
